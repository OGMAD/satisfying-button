import pygame

w_width = 480
w_height = 480

fps = 30

hold = False

clock = pygame.time.Clock()

pygame.init()
pygame.display.set_caption("satisfying button")

win = pygame.display.set_mode((w_width, w_height))


def redraw_game_window():
    global hold
    pygame.draw.rect(win, (0, 0, 100), (w_width / 4, (w_height / 4) + 20, w_width / 2, w_height / 2), border_radius=50)
    pygame.draw.rect(win, (25, 25, 255), (w_width / 4, w_height / 4, w_width / 2, w_height / 2), border_radius=50)

    if not hold:
        pygame.draw.circle(win, (150, 0, 0), (w_width / 2, w_height / 2), 100)
        pygame.draw.circle(win, (255, 25, 25), (w_width / 2, (w_height / 2) - 10), 100)
    else:
        pygame.draw.circle(win, (150, 0, 0), (w_width / 2, w_height / 2), 100)
        pygame.draw.circle(win, (255, 25, 25), (w_width / 2, (w_height / 2) - 5), 100)

    pygame.display.update()


def event_listener():
    global hold
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            return
        elif event.type == pygame.MOUSEBUTTONDOWN:
            hold = True
            pygame.mixer.Sound("button.mp3").play()
        elif event.type == pygame.MOUSEBUTTONUP:
            hold = False


# main game loop
while True:
    event_listener()
    redraw_game_window()
    win.fill((155, 155, 155))
    clock.tick(fps)
